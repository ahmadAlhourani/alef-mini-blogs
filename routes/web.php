<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\BlogPostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
    // return view('welcome');
});

Auth::routes();

Route::get('/home', [BlogPostController::class, 'index'])->name('home');


Route::middleware('auth')->group(function () {
    // profile routes
    Route::prefix('profile')->group(function () {
        Route::get('/', [ProfileController::class, 'show'])->name('profile');
        Route::get('/edit', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::put('/', [ProfileController::class, 'update'])->name('profile.update');
        Route::delete('/', [ProfileController::class, 'destroy'])->name('profile.destroy');
    });

    Route::prefix('posts')->group(function () {
        Route::get('/', [BlogPostController::class, 'myPosts'])->name('my-posts.index');
        Route::get('/create', [BlogPostController::class, 'create'])->name('blog-posts.create');
        Route::post('/', [BlogPostController::class, 'store'])->name('blog-posts.store');
        Route::get('/{blogPost}', [BlogPostController::class, 'show'])->name('blog-posts.show');
        Route::get('/{blogPost}/edit', [BlogPostController::class, 'edit'])->name('blog-posts.edit');
        Route::put('//{blogPost}', [BlogPostController::class, 'update'])->name('blog-posts.update');
        Route::delete('/{blogPost}', [BlogPostController::class, 'destroy'])->name('blog-posts.destroy');
    });
});