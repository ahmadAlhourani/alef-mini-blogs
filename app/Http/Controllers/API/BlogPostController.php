<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIController;
use Illuminate\Http\Request;
use App\Models\BlogPost;
use Validator;
use OpenApi\Annotations as OA;

/**
 * @OA\Tag(
 *     name="Blog Posts",
 *     description="Endpoints for managing blog posts"
 * )
 */

 /**
 * @OA\Schema(
 *     schema="BlogPost",
 *     title="BlogPost",
 *     @OA\Property(
 *         property="title",
 *         type="string",
 *         description="The title of the blog post"
 *     ),
 *     @OA\Property(
 *         property="created_at",
 *         type="string",
 *         description="The creation date of the blog post"
 *     )
 * )
 */
class BlogPostController extends APIController
{
    /**
     * @OA\Get(
     *     path="/api/blog-posts",
     *     tags={"Blog Posts"},
     *     summary="Get all blog posts",
     *     operationId="index",
     *     security={{"bearerAuth": {}}},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/BlogPost"))
     *     )
     * )
     */
    public function profilePosts(Request $request)
    {
        $user = auth()->user();

        $posts = $user->blogPosts()->select('title', 'created_at')->get();
        return response()->json(['data' => $posts], 200);
    }


}