<?php

namespace App\Http\Controllers;

use App\Models\BlogPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogPostController extends Controller
{
    public function index()
    {
        // dd(111);
        $blogPosts = BlogPost::all();
        return view('blog-posts.index', compact('blogPosts'));
    }

    public function myPosts()
    {
        // dd(222);
        // $blogPosts = BlogPost::all();
        $blogPosts = BlogPost::where('user_id',Auth::id())->get();
        return view('blog-posts.index', compact('blogPosts'));
    }

    public function create()
    {
        return view('blog-posts.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required|string',
        ]);

        // dd($request->all());
        $validatedData['user_id'] = Auth::id();

        BlogPost::create($validatedData);

        return redirect()->route('my-posts.index')->with('success', 'Blog post created successfully.');
    }

    public function show(BlogPost $blogPost)
    {
        return view('blog-posts.show', compact('blogPost'));
    }

    public function edit(BlogPost $blogPost)
    {
        return view('blog-posts.edit', compact('blogPost'));
    }

    public function update(Request $request, BlogPost $blogPost)
    {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required|string',
        ]);

        $blogPost->update($validatedData);

        return redirect()->route('my-posts.index')->with('success', 'Blog post updated successfully.');
    }

    public function destroy(BlogPost $blogPost)
    {
        $blogPost->delete();

        return redirect()->route('my-posts.index')->with('success', 'Blog post deleted successfully.');
    }
}