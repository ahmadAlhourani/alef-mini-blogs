<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Tonysm\RichTextLaravel\Models\Traits\HasRichText;
use App\Models\User;
class BlogPost extends Model
{
    use HasFactory,HasRichText;

    // protected $fillable=[
    //     'title',
    //     'body',
    //     'user_id',
    // ];

    protected $guarded = [];

    protected $richTextAttributes = [
        'body',
    ];

    public function user()
{
    return $this->belongsTo(User::class);
}
}