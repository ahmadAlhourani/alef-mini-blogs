<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\BlogPost;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $user = User::firstOrCreate(
            ['email' => 'user@alef.com'],
            [
                'name' => 'Test User',
                'password' => Hash::make('password'),
            ]
        );

        $user->blogPosts()->createMany(
            BlogPost::factory()->count(5)->raw()
        );
    }
}