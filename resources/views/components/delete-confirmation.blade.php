@props(['route', 'formId' => 'delete-form', 'linkText' => __('Delete'), 'message' => __('Are you sure you want to
delete?')])

<div>
    <a href="#"
        onclick="event.preventDefault(); if (confirm('{{ $message }}')) { document.getElementById('{{ $formId }}').submit(); }"
        class="dropdown-item">
        {{ $linkText }}
    </a>


    <form id="{{ $formId }}" action="{{ $route }}" method="POST" style="display: none;">
        @csrf
        @method('DELETE')
    </form>
</div>