@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0">{{ $blogPost->title }}</h5>
                    <small class="text-muted">by {{ $blogPost->user->name }}</small>
                </div>
                <div class="card-body">
                    {!! $blogPost->body !!}
                </div>
                <div class="card-footer text-muted">
                    Created: {{ $blogPost->created_at->format('M d, Y') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection