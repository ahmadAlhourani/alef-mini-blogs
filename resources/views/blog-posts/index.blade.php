@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    @if (auth()->id())
    <div class="row justify-content-center mb-3">
        <div class="col-md-8">
            <a href="{{ route('blog-posts.create') }}" class="btn btn-primary">Create New Blog Post</a>
        </div>
    </div>
    @endif
    @endauth
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Blog Posts') }}</div>

                <div class="card-body">
                    <div class="row row-cols-1 row-cols-md-2 g-4">
                        @foreach ($blogPosts as $post)


                        <div class="col">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $post->title }}</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Author: {{ $post->user->name }}</h6>
                                    <p class="card-text">{{ Str::limit(strip_tags($post->body), 25) }}</p>
                                </div>
                                <div class="card-footer text-muted">
                                    Created: {{ $post->created_at->format('M d, Y') }}
                                </div>
                                <div class="card-footer">
                                    <a href="{{ route('blog-posts.show', $post) }}" class="btn btn-info">View</a>
                                    @auth
                                    @if ($post->user_id === auth()->id())
                                    <a href="{{ route('blog-posts.edit', $post) }}" class="btn btn-secondary">Edit</a>

                                    <form action="{{ route('blog-posts.destroy', $post) }}" method="POST"
                                        class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>

                                    @endif
                                    @endauth
                                </div>
                            </div>
                        </div>



                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection